.. _release-notes-21.12:

=======================
CMB 21.12 Release Notes
=======================

See also :ref:`release-notes-21.07` for previous changes.

Performance Improvements
========================
* Displaying large complex models in the Resource Browser Panel should be much faster.
* Toggling the visibility got large number of model components in the Resource Browser should be much faster.

Improving qtAssociation2ColumnWidget's Operation Handler
--------------------------------------------------------

The original code would cause a large number of refreshes due to the fact it merely would see if the were any resource changes.  The new logic is a bit more intelligent and will only cause a refresh if any of the following conditions happen:

* A modified component is either the widget's attribute or can be associated with the widget's attribute
* A deleted component could have been associated with the widget's attribute
* A created component could be associated with the widget's attribute

The result should be a more responsive UI.

Display hint for attribute resources
------------------------------------
CMB ModelBuilder now supports version 5 SMTK XML files that include the "DisplayHint" XML attribute for Attribute Resources.
Only those Attribute Resource with this hint will be automatically displayed in the attribute editor panel when loaded.  Older XML files will still automatically display themselves in the attribute editor panel when loaded.  This can result in speed improvements in CMB applications that load in multiple SBT files at the same time.

UI Improvements
===============

Changes in Displaying Attribute Group Items
--------------------------------------------
Empty Group Items will no longer display an empty frame.

Changes in Displaying Attribute Views
-------------------------------------

Hiding "Advance Level" Definitions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Attribute Views will now hide any attribute definitions
that have an advance level that is higher than the user's
active advance level.  This enables the Attribute View to hide
itself if all its definitions should be hidden from the user.

Improving UI handling of Signal Operations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Originally the qtAttributeView class would ignore the Signal Operation since typically it would be the only Qt UI element that would be creating, removing, and changing the Attributes it is displaying.  However, this prevented the UI designer from having AttributeViews that displayed the same information from being used in Selector Views or have different AttributeViews overlap their contents (for example one View could be displaying Fluid Boundary Conditions, while another was displaying all Boundary Conditions)

This change now encodes the address of the View that initiated the change so that we can avoid a View from being updated via a Signal Operation that it itself initiated.

qtAttributeView has now been updated to only ignore Signal Operations that it triggered.

Supporting smtk.extensions.attribute_view.name_read_only in qtAttributeViews
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You can now indicate that an Attribute's name should not be modified by creating a bool Property on the Attribute called: **smtk.extensions.attribute_view.name_read_only** and setting its value to true.


Added Ability to Set Attribute Editor Panel's Title
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Attribute Editor Panel name can now be configure by a smtk::view::Configuration.

If the Configuration is Top Level then the following Configuration Attributes can be used:

* AttributePanelTitle - defines the base name of the Panel.  If not specified it defaults to Attribute Editor.
* IncludeResourceNameInPanel - if specified and set to true, the Panel's title will include the name of the resource in ()

SimpleAttribute.sbt contains an example:

.. code-block:: xml

  <Views>
    <View Type="Attribute" Title="External Expression Test Pt - Source" TopLevel="true" DisableTopButtons="false"
      AttributePanelTitle="SMTK Test" IncludeResourceNameInPanel="t">
      <AttributeTypes>
        <Att Type="B-expressions"/>
      </AttributeTypes>
    </View>
  </Views>

Post-processing control moved to SMTK
-------------------------------------

In order for SMTK's attribute panel to modify its behavior depending on
whether CMB is in post-processing mode or not, the state variable
that controls post-processing mode has been moved into SMTK's pqSMTKBehavior
class.

The toolbar button and UI code to effect post-processing mode remain in
this repository (CMB). However, now it is possible for other SMTK
elements to initiate post-processing mode.

SMTK Related Changes
====================
For additional information on changes to SMTK, please see [SMTK-21.12](https://smtk.readthedocs.io/en/latest/release/smtk-21.12.html)
