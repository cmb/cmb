.. _release-notes-23.01:

=======================
CMB 23.01 Release Notes
=======================

See also :ref:`release-notes-22.04` for previous changes.

CMB 23.01 is based on SMTK 23.01 as well as ParaView 5.11!

Changing to CMB CI Testing
==========================
CMB now uses MSVC 2022 to test merge requests rather than MSVC 2019.


SMTK Related Changes
====================
For additional information on changes to SMTK, please see `SMTK-23.01 <https://smtk.readthedocs.io/en/latest/release/smtk-23.01.html>`_.

ParaView Related Changes
====================
For additional information on changes to ParaView, please see `ParaView-5.11 <https://www.kitware.com/paraview-5-11-0-release-notes/>`_.
