# Linux-specific builder configurations and build commands

## Base images

### Fedora

.fedora41:
    # Xcode 16.1 update; synchronize with other platforms
    image: "kitware/cmb:ci-cmb-fedora41-20250226"

    variables:
        GIT_SUBMODULE_STRATEGY: recursive
        GIT_CLONE_PATH: $CI_BUILDS_DIR/gitlab-kitware-sciviz-ci

### Lint builds

.fedora41_tidy:
    extends: .fedora41

    variables:
        CMAKE_CONFIGURATION: fedora41_tidy
        CTEST_NO_WARNINGS_ALLOWED: 1

.fedora41_memcheck:
    extends: .fedora41

    variables:
        CMAKE_BUILD_TYPE: RelWithDebInfo

.fedora41_asan:
    extends: .fedora41_memcheck

    variables:
        CMAKE_CONFIGURATION: fedora41_asan
        CTEST_MEMORYCHECK_TYPE: AddressSanitizer
        # Disable LeakSanitizer for now. It's catching all kinds of errors that
        # need investigated or suppressed.
        CTEST_MEMORYCHECK_SANITIZER_OPTIONS: detect_leaks=0

.fedora41_ubsan:
    extends: .fedora41_memcheck

    variables:
        CMAKE_CONFIGURATION: fedora41_ubsan
        CTEST_MEMORYCHECK_TYPE: UndefinedBehaviorSanitizer

.fedora41_coverage:
    extends: .fedora41

    variables:
        CMAKE_BUILD_TYPE: Debug
        CMAKE_CONFIGURATION: fedora41_coverage
        CTEST_COVERAGE: 1
        CMAKE_GENERATOR: Unix Makefiles

### Build and test

.fedora41_plain:
    extends: .fedora41

    variables:
        CMAKE_CONFIGURATION: fedora41_plain

## Tags

.linux_builder_tags:
    tags:
        - build
        - cmb
        - docker
        - linux-x86_64

.linux_test_tags:
    tags:
        - cmb
        - docker
        - linux-x86_64
        - x11

.linux_test_priv_tags:
    tags:
        - cmb
        - docker
        - linux-x86_64
        - privileged
        - x11

## Linux-specific scripts

.before_script_linux: &before_script_linux
    - .gitlab/ci/cmake.sh
    - .gitlab/ci/ninja.sh
    - export PATH=$PWD/.gitlab:$PWD/.gitlab/cmake/bin:$PATH
    - cmake --version
    - ninja --version

.cmake_build_linux:
    stage: build

    script:
        - *before_script_linux
        - .gitlab/ci/sccache.sh
        - sccache --start-server
        - sccache --show-stats
        - "$LAUNCHER ctest -V -S .gitlab/ci/ctest_configure.cmake"
        - "$LAUNCHER ctest -V -S .gitlab/ci/ctest_build.cmake"
        - sccache --show-stats
    interruptible: true

.cmake_build_linux_tidy:
    stage: build

    script:
        - *before_script_linux
        - dnf install -y --setopt=install_weak_deps=False clang-tools-extra
        - .gitlab/ci/sccache.sh
        - sccache --start-server
        - sccache --show-stats
        - "$LAUNCHER ctest -V -S .gitlab/ci/ctest_configure.cmake"
        - "$LAUNCHER ctest -V -S .gitlab/ci/ctest_build.cmake"
        - sccache --show-stats
    interruptible: true

.cmake_test_linux:
    stage: test

    script:
        - *before_script_linux
        - "$LAUNCHER ctest --output-on-failure -V -S .gitlab/ci/ctest_test.cmake"
    interruptible: true

.cmake_memcheck_linux:
    stage: test

    script:
        - *before_script_linux
        - "$LAUNCHER ctest --output-on-failure -V -S .gitlab/ci/ctest_memcheck.cmake"
    interruptible: true

.cmake_coverage_linux:
    stage: analyze

    script:
        - *before_script_linux
        - "$LAUNCHER ctest --output-on-failure -V -S .gitlab/ci/ctest_coverage.cmake"
    coverage: '/Percentage Coverage: \d+.\d+%/'
    interruptible: true
