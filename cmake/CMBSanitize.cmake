#=========================================================================
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
#=========================================================================

# This code has been adapted from remus (https://gitlab.kitware.com/cmb/remus)

if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang" OR
   CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang")
  set(CMAKE_COMPILER_IS_CLANGXX 1)
endif()

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_COMPILER_IS_CLANGXX)

  include(CheckCXXCompilerFlag)

  #Add option for enabling sanitizers
  option(cmb_enable_sanitizer "Build with sanitizer support." OFF)
  mark_as_advanced(cmb_enable_sanitizer)

  if(cmb_enable_sanitizer)
    set(cmb_sanitizer "address"
      CACHE STRING "The sanitizer to use")
    mark_as_advanced(cmb_sanitizer)

    #We're setting the CXX flags and C flags beacuse they're propagated down
    #independent of build type.
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=${cmb_sanitizer}")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fsanitize=${cmb_sanitizer}")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fsanitize=${cmb_sanitizer}")
    set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -fsanitize=${cmb_sanitizer}")
  endif()
endif()
