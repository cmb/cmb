//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
/**
 * @class   cmbObjectFactory
 * @brief   is a factory for implementing derived instances of vtk classes
 *
 * VTK classes can be designed to use the vtkObjectFactory to construct new
 * instances by using the vtkObjectFactoryNewMacro to define the class's New()
 * static method. This pattern allows for upstream libraries (like those coming
 * from ParaView) to implement instances of derived classes (like those coming
 * from ModelBuilder). cmbObjectFactory is a means to inform vtkObjectFactory of
 * derived classes that should be used in favor of their base class
 * implementations.
*/
#ifndef modelbuilder_mbObjectFactory_h
#define modelbuilder_mbObjectFactory_h

#include "vtkObjectFactory.h"

class cmbObjectFactory : public vtkObjectFactory
{
public:
  static cmbObjectFactory* New();
  vtkTypeMacro(cmbObjectFactory, vtkObjectFactory);

  cmbObjectFactory(const cmbObjectFactory&) = delete;
  cmbObjectFactory& operator=(const cmbObjectFactory&) = delete;

  const char* GetDescription() override { return "ModelBuilder factory overrides."; }

  const char* GetVTKSourceVersion() override;

  void PrintSelf(ostream& os, vtkIndent indent) override;

protected:
  cmbObjectFactory();
};

#endif
