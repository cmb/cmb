//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef cmbLoadDataReaction_h
#define cmbLoadDataReaction_h

#include "pqLoadDataReaction.h"
#include "vtkSMTrace.h"

/**\brief A pqLoadDataReaction subclass to help with python tracing.
  */
class cmbLoadDataReaction : public pqLoadDataReaction
{
  Q_OBJECT
  using Superclass = pqLoadDataReaction;

public:
  cmbLoadDataReaction(QAction* parent)
    : Superclass(parent)
  {
  }

protected:
  /**
   * Called when the action is triggered.
   */
  void onTriggered() override
  {
    // Suppress tracing of standard pipeline reader in favor of tracing SMTK operation.
    SM_SCOPED_TRACE(TraceText).arg("# File .. Open");
    pqLoadDataReaction::onTriggered();
  }
};
#endif // cmbLoadDataReaction_h
