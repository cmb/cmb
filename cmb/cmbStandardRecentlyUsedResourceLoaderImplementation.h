//=============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=============================================================================
#ifndef cmbStandardRecentlyUsedResourceLoaderImplementation_h
#define cmbStandardRecentlyUsedResourceLoaderImplementation_h

#include "pqStandardRecentlyUsedResourceLoaderImplementation.h"
#include "vtkSMTrace.h"

#include <QObject> // needed for QObject

/**
 * @brief standard ParaView recent file loader, with python tracing of load
 * suppressed.
 */
class cmbStandardRecentlyUsedResourceLoaderImplementation
  : public pqStandardRecentlyUsedResourceLoaderImplementation
{
  Q_OBJECT
  Q_INTERFACES(pqRecentlyUsedResourceLoaderInterface)

  using Superclass = pqStandardRecentlyUsedResourceLoaderImplementation;

public:
  cmbStandardRecentlyUsedResourceLoaderImplementation(QObject* parent = nullptr)
    : Superclass(parent)
  {
  }

  bool load(const pqServerResource& resource, pqServer* server) override
  {
    // Suppress tracing of standard pipeline reader in favor of tracing SMTK operation.
    SM_SCOPED_TRACE(TraceText).arg("# File .. Open Recent");
    return pqStandardRecentlyUsedResourceLoaderImplementation::load(resource, server);
  }

private:
  Q_DISABLE_COPY(cmbStandardRecentlyUsedResourceLoaderImplementation)
};

#endif
