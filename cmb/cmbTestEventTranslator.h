//=============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=============================================================================
#ifndef modelbuilder_mbTestEventTranslator_h
#define modelbuilder_mbTestEventTranslator_h

#include "pqWidgetEventTranslator.h"

#include <QPointer>

class pqFileDialog;

/** \brief Translates low-level Qt events into high-level ParaView events.
  *
  * This makes tests less fragile.
  *
  * \sa pqEventTranslator
  */
class cmbTestEventTranslator : public pqWidgetEventTranslator
{
  Q_OBJECT

public:
  cmbTestEventTranslator(QObject* p = nullptr);

  bool translateEvent(QObject* Object, QEvent* Event, bool& Error) override;

  cmbTestEventTranslator(const cmbTestEventTranslator&) = delete;
  cmbTestEventTranslator& operator=(const cmbTestEventTranslator&) = delete;

private:
  QPointer<pqFileDialog> m_currentObject;

private Q_SLOTS:
  void onFilesSelected(const QString& /*file*/);
  void onCancelled();
};

#endif
