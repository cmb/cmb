//=============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=============================================================================
#ifndef mbLayoutSpec_h
#define mbLayoutSpec_h

#include <QDockWidget>
#include <QMainWindow>

class mbLayoutSpec
{
public:
  mbLayoutSpec() = default;
  virtual ~mbLayoutSpec() = default;

  // Implementation of the layout specification for QDockWidgets in the QMainWindow
  virtual void layoutDockWidget(QMainWindow* mw, QDockWidget* dw);
};

#endif // mbLayoutSpec_h
