cmake_minimum_required(VERSION 3.13)

#If the user/superbuild hasn't explicitly stated what c++ standard to use
#require C++17
if(NOT DEFINED CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 17)
  set(CMAKE_CXX_STANDARD_REQUIRED True)
  set(CMAKE_CXX_EXTENSIONS FALSE)
endif()

option(BUILD_SHARED_LIBS "Build CMB using shared libraries" ON)

################################################################################
# CMB version number
include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/CMBVersion.cmake)
cmb_parse_version_file(cmb
  EXCLUDE_PATCH_EXTRA)

project(cmb VERSION ${cmb_version})
list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

include(GNUInstallDirs)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_BINDIR}")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR}")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR}")

find_package(smtk REQUIRED)
find_package(ParaView REQUIRED)
find_package(Qt5 REQUIRED COMPONENTS Core Widgets)

foreach (plugin_dependency IN LISTS CMB_EXTRA_SMTK_PLUGINS)
  find_package("${plugin_dependency}" REQUIRED)
endforeach ()

include(CMBPreamble) # Determine architecture, set up build-output directories.
include(CMBTesting) # Add an option to enable testing and set up pointers to the test data.
include(CMBCoverage) # Add options for performing code coverage tests.
include(CMBSanitize) # Add options for performing code sanitization.
include(CMBMacros)

# Python is already required, so let the internal python shell function by default.
option(cmb_enable_pythonshell "Enable the Python Shell" ON)
option(cmb_enable_multiservers "Enable --multi-servers by default" OFF)
option(cmb_enable_objectpicking "Enable picking of smtk entities" OFF)

set(cmb_plugin_paths
  "${cmb_plugin_paths}"
  "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}"
  CACHE STRING "Semi-colon separated paths for testing plugins."
)
mark_as_advanced(cmb_plugin_paths)

set(APPLICATION_ROOT modelbuilder CACHE PATH "Root containing application configuration")
get_filename_component(application_bin "/prefix/${APPLICATION_ROOT}" NAME)

# Configure the application  at APPLICATION_ROOT
add_subdirectory(${APPLICATION_ROOT} ${application_bin})
include_directories(${APPLICATION_ROOT})
include_directories(${application_bin})

add_subdirectory(thirdparty)
add_subdirectory(plugins) # Plugins must be built before the app so .plugins file will be correct.
add_subdirectory(cmb)

get_property(_cmb_application_NAME GLOBAL PROPERTY _cmb_application_NAME)
# Do not report some warnings from generated code to the dashboard:
configure_file(
  "${CMAKE_CURRENT_SOURCE_DIR}/CTestCustom.cmake.in"
  "${CMAKE_CURRENT_BINARY_DIR}/CTestCustom.cmake")

option(cmb_enable_documentation "Include targets Sphinx-generated documentation" OFF)
if (cmb_enable_documentation)
  find_package(Sphinx)
  add_subdirectory(doc)
endif()

# Add rules to build packages
include(CMBPackaging)
